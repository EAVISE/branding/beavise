# The Beavise Beamer theme
A minimalistic presentation theme with the same look and feel as the [EAVISE logo itself](https://gitlab.com/EAVISE/logo).

Please take a look at [main.pdf](./main.pdf) for an overview of the theme's features.